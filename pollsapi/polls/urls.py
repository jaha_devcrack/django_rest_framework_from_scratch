from django.urls import include, re_path
from django.urls import path
from rest_framework.routers import DefaultRouter

#from .views import polls_list, polls_detail
from .apiviews import PollList, PollDetail
from .apiviews import ChoiceList, CreateVote, PollViewSet
from .apiviews import UserCreate
from .apiviews import LoginView
from  rest_framework.authtoken import views

router = DefaultRouter()
router.register('polls', PollViewSet, base_name='polls')

urlpatterns = [
    path('choices/<int:pk>/', ChoiceList.as_view(), name='choice_list'),
    path('vote/', CreateVote.as_view(), name='create_vote'),
    path('polls/<int:pk>/choices/', ChoiceList.as_view(), name='choice_list'),
    path('polls/<int:pk>/choices/<int:choice_pk>/vote/', CreateVote.as_view(), name='create_vote'),
    path('users/', UserCreate.as_view(), name='user_create'),
    path('login/', views.obtain_auth_token, name='login'),
]

# urlpatterns = [
#     path('choices/<int:pk>/', ChoiceList.as_view(), name='choice_list'),
#     path('vote/', CreateVote.as_view(), name='create_vote'),
#     path('polls/<int:pk>/choices/', ChoiceList.as_view(), name='choice_list'),
#     path('polls/<int:pk>/choices/<int:choice_pk>/vote/', CreateVote.as_view(), name='create_vote'),
#     path('users/', UserCreate.as_view(), name='user_create'),
#     path('login/', LoginView.as_view(), name='login'),
# ]
urlpatterns += router.urls

# urlpatterns = [
#     path('choices/', ChoiceList.as_view(), name='choice_list'),
#     path('vote/', CreateVote.as_view(), name='create_vote'),
#     path('polls/', PollList.as_view(), name='poll_list'),
#     path('polls/<int:pk>/', PollDetail.as_view(), name='polls_detail'),
# ]


